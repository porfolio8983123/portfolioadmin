import React, { useEffect } from 'react'
import './ProjectDetails.css';
import {AiFillProject, AiFillSafetyCertificate} from 'react-icons/ai';
import { usePortfolioContext } from '../../hooks/ProjectContext';
import { useCertificateContext } from '../../hooks/CertificateContext';

function ProjectDetails() {
    const {projectCount,dispatch} = usePortfolioContext()
    const {certificates,certificateCount,dispatchCertificate} = useCertificateContext();

    console.log(certificates,certificateCount)

    const fetchProject = async () => {
        const response = await fetch(`https://portfolioadmin.onrender.com/api/portfolio`)
        const parseRes = await response.json();

        if (response.ok) {
            dispatch({type: 'SET_PROJECT', payload: parseRes})
        }
    }

    const fetchCertificates = async () => {
        const response = await fetch(`https://portfolioadmin.onrender.com/api/portfolio/certificate`);
        const parseRes = await response.json();

        console.log("response ", parseRes)

        if (response.ok) {
            dispatchCertificate({type:'SET_CERTIFICATE',payload:parseRes})
        }
    }

    useEffect(() => {
        fetchProject();
        fetchCertificates();
    },[dispatch,dispatchCertificate,fetchCertificates,fetchProject])

  return (
    <section>
        <div className='container details__container'>
            <div className='details__items'>
                <h5>Cummulative</h5>
                <div className='description'>
                    <AiFillProject/>
                    <h3>Projects</h3>
                </div>
                <p className='numbers'>{projectCount}</p>
            </div>
            <div className='details__items'>
                <h5>Cummulative</h5>
                <div className='description'>
                    <AiFillSafetyCertificate
                        color='white'
                    />
                    <h3>Certificates</h3>
                </div>
                <p className='numbers'>{certificateCount}</p>
            </div>
        </div>
    </section>
  )
}

export default ProjectDetails