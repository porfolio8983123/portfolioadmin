import React from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";

import "./certificate.css";

import { Navigation } from "swiper";
import { useCertificateContext } from '../../hooks/CertificateContext';


function Certificates() {
    const {certificates,dispatchCertificate} = useCertificateContext();

    const deleting = async (id) => {
        console.log(id)
        try {
            const certificate = await fetch(`https://portfolioadmin.onrender.com/api/portfolio/certificate/${id}`,{
                method:'DELETE'
            })

            const parseRes = await certificate.json();

            if (certificate.ok) {
                dispatchCertificate({type:'DELETE_CERTIFICATE',payload:parseRes})
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    const handleClick = (id) => {
        const comfirmed = window.confirm("Are you sure you want to delete?")
        if (comfirmed) {
            deleting(id)
        }
    }

    return (
        <section>
            <div className='scontainer'>
            <h2 style={{textAlign:'center', marginBottom:40}}>Certificates</h2>
                <Swiper
                     navigation={true} modules={[Navigation]} className="swiper"
                    >
                        {certificates &&
                            certificates.map((certificate) => (
                                <SwiperSlide className='swiper-slide' style={{position:'relative'}} key={certificate._id}>
                                <img src = "https://firebasestorage.googleapis.com/v0/b/porfolio-a7f65.appspot.com/o/machine%20learning%20level%201.jpg?alt=media&token=32baf0e9-1a28-4a1d-8779-403d4916dee6" alt=''/>
                                <button onClick={() => handleClick(certificate._id)} className='btn btn-primary' style={{position: "absolute",top:0,right:150 }}>Delete</button>
                            </SwiperSlide>
                            ))
                        }
                </Swiper>
            </div>
        </section>
    )
}

export default Certificates