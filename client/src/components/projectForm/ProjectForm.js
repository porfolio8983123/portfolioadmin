import React, { useState } from 'react'
import './ProjectForm.css';
import CertificateForm from '../certificateForm/CertificateForm';
import { usePortfolioContext } from '../../hooks/ProjectContext';

function ProjectForm() {

    const {dispatch} = usePortfolioContext();

    const [title, setTitle] = useState('');
    const [url,setUrl] = useState('');
    const [image,setImage] = useState('');
    const [gitLink, setGitLink] = useState('');
    const handleProjectSubmit = async (e) => {
        e.preventDefault();

        const project = {title,url,image,gitLink}

        const response = await fetch(`https://portfolioadmin.onrender.com/api/portfolio`, {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(project)
        })

        const parseRes = await response.json();

        if (response.ok) {
            setTitle('')
            setUrl('')
            setImage('')
            setGitLink('')
            console.log("New project Added ", parseRes)
            const payloadData = {project:parseRes,certificate:[]}
            dispatch({type:'CREATE_PROJECT',payload:payloadData})
        }

    }

    return (
        <section className='projectform'>
            <h2 className='container title'>Add your merits here</h2>
            <div className='container form__container'>
                <div>
                    <form onSubmit={handleProjectSubmit}>
                        <h3>Project Details</h3>
                        <input type='text' name = "title" placeholder='Project Title' required
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                        <input type='text' name = "url" placeholder='Project Url' required
                            value={url}
                            onChange={(e) => setUrl(e.target.value)}
                        />
                        <input type='text' name = "image" placeholder='Image Link' required
                            value={image}
                            onChange={(e) => setImage(e.target.value)}
                        />
                        <input type='text' name = "gitlab" placeholder='GitLab Url' required
                            value={gitLink}
                            onChange={(e) => setGitLink(e.target.value)}
                        />
                        <button className='btn btn-primary'>Add Project</button>
                    </form>
                </div>
                <div>
                    <CertificateForm/>
                </div>
            </div>
        </section>
    )
}

export default ProjectForm