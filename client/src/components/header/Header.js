import React from 'react'
import {RiLogoutCircleRLine} from 'react-icons/ri';
import './Header.css';

function Header({setAuth}) {
  return (
    <header>
        <div className='container header__container'>
            <h1>Dashboard</h1>
            <RiLogoutCircleRLine
                className='logout__icon'
                size={30} 
                onClick={() => setAuth(false)}
            />
        </div>
    </header>
  )
}

export default Header