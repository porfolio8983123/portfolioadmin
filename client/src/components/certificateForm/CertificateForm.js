import React, { useState } from 'react';
import { useCertificateContext } from '../../hooks/CertificateContext';

function CertificateForm() {

  const {dispatchCertificate} = useCertificateContext();

  const [title,setTitle] = useState("");
  const [image, setImage] = useState("");

  const handleCertificateSubmit = async (e) => {
    e.preventDefault();

    const certificate = {title,image}

    const response = await fetch(`https://portfolioadmin.onrender.com/api/portfolio/certificate`, {
      method:'POST',
      headers: {'Content-Type':'application/json'},
      body:JSON.stringify(certificate)
    })

    const parseRes = await response.json();

    if (response.ok) {
      setTitle('')
      setImage('')
      dispatchCertificate({type:'CREATE_CERTIFICATE', payload:parseRes})
    } 
  }

  return (
    <div>
        <form onSubmit={handleCertificateSubmit}>
            <h3>Certificate Details</h3>
            <input type='text' name = "title" placeholder='Certificate Name' required
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <input type='text' name = "image" placeholder='Image Link' required
              value={image}
              onChange={(e) => setImage(e.target.value)}
            />
            <button className='btn btn-primary'>Add Certificate</button>
        </form>
    </div>

  )
}

export default CertificateForm