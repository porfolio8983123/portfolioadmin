const mongoose = require('mongoose')
const Schema = mongoose.mongoose.Schema;

const certificateSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    image: {
        type: String,
        required: true
    }
},{timestamps: true})

module.exports = mongoose.model("certificate",certificateSchema)