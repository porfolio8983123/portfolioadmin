const express = require('express');
require('dotenv').config();
const cors = require('cors');
const mongoose = require('mongoose');
const porfolioRoutes = require('./routes/portfolio');

const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req,res) => {
    res.json("Fine")
})

app.use('/api/portfolio',porfolioRoutes)

mongoose.connect(process.env.MONGO_URI)
.then(() => {
    app.listen(process.env.PORT,() => {
        console.log("Listening to 5000")
    })
}).catch((error) => {
    console.log(error.message);
})